# Markov Chains Word Generator

### Генератор текстов на основе N-грамм. 
- модель обёрнута в класс и соблюдает принципы ООП;
- для взаимодействия с моделью реализованы отдельные консольные скрипты (на основе _**argparse**_);
- для обучения модели можно ввести текст в консоль либо указать путь к директории с .txt файлами
- модель использует **биграммы**, **триграммы** и **квадрограмммы**, причём, для разнообразия текста, последние задействуются для генерации **только в случае**, когда есть несколько вариантов для выбора;
- модель способна расставлять знаки препинания;
- модель, конечно, ломает все шаблоны, но в [своем ИИ-боте](https://t.me/uu_chatbot) я всё равно использовал GPT-3.5 :)


## English description
An utility that is trained on the corpus of texts and generates new ones. The model is based on n-grams (Markov chains) and it uses bigrams, trigrams and quadrograms. The model was trained on various artworks (like books and etc). The model has not been uploaded to the repository - it exceeds the 25 MB size limit; the texts of artworks have been uploaded. The program analyzes and generates texts consisting of Russian words separated by a space and a hyphen. Train scripts.py and generate.py they access the wordgenerator module and work through the console:

train.py - model training. The script generates n-grams and saves the result - an instance of the class with attributes - via pickle. Arguments: a) --input_dir - the path to the directory with training texts (in the absence of an argument, it will request text from the console); c) --model - the path to save the model (by default, the current directory);
generate.py - text generation. Loads the model.pkl file and starts the generation method. Arguments: a) --model - the path from which the model is loaded; c) --prefix - the prefix, i.e. the initial phrase that will be used for generation; c) --length - the length of the generated text, set in words.
